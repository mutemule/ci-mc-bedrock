# Container Image for a Bedrock (aka Microsoft) Minecraft server

This creates a container image for a Minecraft Bedrock server, that can be
used to provide a Minecraft world for modern Minecraft clients. This will
_not_ work with vanilla clients.

# Objective

Most publicly-built container images tend to be bloated and insecure,
depending on services like S6 which pull in large base systems, and run
as root with the full Linux capabilities.

This image is small and lean, and is designed to be run requiring no
special privileges at all.

# Requirements

A single volume that can be mounted into both this container, as well
as the dnscrypt-proxy container.

# Using

```sh
docker  run -d \
    --name mc-bedrock \
    --init \
    --volume minecraft-worlds:/worlds \
    --volume minecraft-config:/config \
    --tmpfs /tmp \
    --network games \
    --read-only \
    --cap-drop all \
    --security-opt no-new-privileges \
    --pids-limit 50 \
    registry.gitlab.com/mutemule/ci-mc-bedrock/main:latest > "/var/run/mc-bedrock.cid"
```
