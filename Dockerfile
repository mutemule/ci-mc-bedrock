FROM docker.io/library/debian:bullseye-slim AS builder

RUN apt -y update && \
    apt -y install unzip curl

RUN bedrock_download_url="$(curl -sL -A "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/80.0" https://www.minecraft.net/en-us/download/server/bedrock/ | awk -F\" '/https:\/\/minecraft.azureedge.net\/bin-linux\// {print $2}')" && \
    curl -sL "${bedrock_download_url}" -o /tmp/bedrock.zip && \
    install -m 0755 -o root -g root -d /opt/minecraft && \
    unzip -o /tmp/bedrock.zip -d /opt/minecraft && \
    chmod 0755 /opt/minecraft/bedrock_server


FROM docker.io/library/debian:bullseye-slim

LABEL maintainer="Damian Gerow <dwg@flargle.io>"
LABEL name="minecraft-bedrock"

RUN apt -y update && \
    apt -y upgrade && \
    apt -y install curl && \
    rm -rf /var/lib/apt/lists* /tmp/*

COPY --from=builder /opt/ /opt/

WORKDIR /opt/minecraft

RUN useradd -c "Minecraft" -m -d /home/miecraft -s /sbin/nologin -U minecraft && \
    ln -sf /worlds && \
    ln -sf /config/server.properties && \
    ln -sf /config/permissions.json && \
    ln -sf /config/whitelist.json

EXPOSE 19132/udp

VOLUME /worlds /config

USER minecraft

ENV LD_LIBRARY_PATH="."

ENTRYPOINT [ "/opt/minecraft/bedrock_server" ]
